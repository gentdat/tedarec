<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="2.0"
                xmlns:d="http://thaso.de/RandomTestData/TestDataDescription"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:math="java.lang.Math"
                xmlns:dyn="http://exslt.org/dynamic"
                xmlns:xalan="http://xml.apache.org/xalan"
                exclude-result-prefixes="math dyn xalan"
>

  <xsl:import href="../../main/xsl/gentdat.xsl"/>

  <xsl:output method="text" encoding="utf-8"/>

  <xsl:template match="d:*">
    <xsl:variable name="name" select="name(.)"/>
    <xsl:variable name="mock" select="./ancestor::testinfo/mocks/template[./@apply=$name]"/>
    <xsl:choose>
      <xsl:when test="$mock">
        <xsl:value-of select="$mock/text()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-imports/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="testdata">
    <xsl:choose>
      <xsl:when test="./@file">
        <xsl:copy-of select="document(concat('../xml/assets/',./@file))/*"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="./*"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="when">
    <xsl:param name="nodes"/>

    <xsl:variable name="apply">
      <xsl:choose>
        <xsl:when test="./@apply">
          <xsl:value-of select="./@apply"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>field</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="cmd" select="concat('$nodes/testinfo/',$apply)"/>
    <xsl:variable name="objs" select="dyn:evaluate($cmd)"/>
    <xsl:apply-templates select="$objs"/>
  </xsl:template>

  <xsl:template match="then">
    <xsl:param name="result"/>

    <xsl:variable name="which">
      <xsl:choose>
        <xsl:when test="./@value">
          <xsl:value-of select="./@value"/>
        </xsl:when>
        <xsl:when test="./@count">
          <xsl:value-of select="./@count"/>
        </xsl:when>
        <xsl:when test="./@length">
          <xsl:value-of select="./@length"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="current-cmd">
      <xsl:choose>
        <xsl:when test="./@value">
          <xsl:value-of select="concat('xalan:nodeset($result)/',$which)"/>
        </xsl:when>
        <xsl:when test="./@count">
          <xsl:value-of select="concat('count(xalan:nodeset($result)/',$which,')')"/>
        </xsl:when>
        <xsl:when test="./@length">
          <xsl:value-of select="concat('string-length(xalan:nodeset($result)/',$which,')')"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="current-value" select="dyn:evaluate($current-cmd)"/>

    <xsl:variable name="operator">
      <xsl:choose>
        <xsl:when test="./@is">
          <xsl:text>=</xsl:text>
        </xsl:when>
        <xsl:when test="./@less-then">
          <xsl:text>&lt;</xsl:text>
        </xsl:when>
        <xsl:when test="./@greater-then-or-equal">
          <xsl:text>&gt;=</xsl:text>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="expected-value">
      <xsl:choose>
        <xsl:when test="./@is">
          <xsl:value-of select="./@is"/>
        </xsl:when>
        <xsl:when test="./@less-then">
          <xsl:value-of select="./@less-then"/>
        </xsl:when>
        <xsl:when test="./@greater-then-or-equal">
          <xsl:value-of select="./@greater-then-or-equal"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="assert-result" select="dyn:evaluate(concat('$current-value ',$operator,' $expected-value'))"/>

    <xsl:element name="assert">
      <xsl:choose>
        <xsl:when test="$assert-result">
          <xsl:attribute name="result">
            <xsl:text>OK</xsl:text>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="result">
            <xsl:text>FAILED</xsl:text>
          </xsl:attribute>

          <xsl:text>&#xA;    - </xsl:text>
          <xsl:choose>
            <xsl:when test="./@value">
              <xsl:text>value</xsl:text>
            </xsl:when>
            <xsl:when test="./@count">
              <xsl:text>count</xsl:text>
            </xsl:when>
            <xsl:when test="./@length">
              <xsl:text>length</xsl:text>
            </xsl:when>
          </xsl:choose>
          <xsl:text> of '</xsl:text>
          <xsl:value-of select="$which"/>
          <xsl:text>' (</xsl:text>
          <xsl:value-of select="$current-value"/>
          <xsl:text>) is not </xsl:text>
          <xsl:value-of select="$operator"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="$expected-value"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>

  <xsl:template match="test">
    <xsl:variable name="nodes">
      <xsl:element name="testinfo">
        <xsl:copy-of select="/unit-test/mocks"/>
        <xsl:apply-templates select="./testdata"/>
      </xsl:element>
    </xsl:variable>

    <xsl:variable name="result">
      <xsl:apply-templates select="./when">
        <xsl:with-param name="nodes" select="xalan:nodeset($nodes)"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:variable name="assert">
      <xsl:apply-templates select="./then">
        <xsl:with-param name="result" select="$result"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:element name="test">
      <xsl:attribute name="name">
        <xsl:value-of select="./@name"/>
      </xsl:attribute>
      <xsl:copy-of select="xalan:nodeset($assert)"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="unit-test">
    <xsl:variable name="results">
      <xsl:element name="unit-test">
        <xsl:apply-templates select="./test"/>
      </xsl:element>
    </xsl:variable>
    <xsl:copy-of select="xalan:nodeset($results)"/>
  </xsl:template>

  <xsl:template match="/">
    <xsl:variable name="results">
      <xsl:apply-templates select="/unit-test"/>
    </xsl:variable>

    <xsl:apply-templates select="xalan:nodeset($results)/unit-test" mode="print"/>

    <xsl:if test="xalan:nodeset($results)/unit-test/test/assert/@result='FAILED'">
      <xsl:message terminate="yes">
        <xsl:text>UNIT TEST FAILED !!!</xsl:text>
      </xsl:message>
    </xsl:if>
  </xsl:template>

  <xsl:template match="unit-test" mode="print">
    <xsl:apply-templates select="./test" mode="print"/>
    <xsl:text>tests: </xsl:text>
    <xsl:value-of select="count(./test)"/>
    <xsl:text> - failed: </xsl:text>
    <xsl:value-of select="count(./test[./assert/@result='FAILED'])"/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="test" mode="print">
    <xsl:text>Test: </xsl:text>
    <xsl:value-of select="./@name"/>
    <xsl:text> - </xsl:text>
    <xsl:choose>
      <xsl:when test="./assert/@result='FAILED'">
        <xsl:text>FAILED</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>OK</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:apply-templates select="./assert" mode="print"/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="assert" mode="print">
    <xsl:if test="./@result='FAILED'">
      <xsl:value-of select="./text()"/>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
