<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:d="http://thaso.de/RandomTestData/TestDataDescription"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:math="java.lang.Math"
                exclude-result-prefixes="math"
>

  <xsl:output method="xml" version="1.0" encoding="utf-8"/>

  <xsl:template name="random-digit">
    <xsl:param name="size"/>
    <xsl:if test="$size &gt; 0">
      <xsl:value-of select="math:floor(math:random() * 10) mod 10"/>
      <xsl:if test="$size &gt; 1">
        <xsl:call-template name="random-digit">
          <xsl:with-param name="size" select="$size - 1"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template match="d:size">
    <xsl:call-template name="random-digit">
      <xsl:with-param name="size" select="./@value"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="d:range">
    <xsl:variable name="range" select="./@to - ./@from"/>
    <xsl:value-of select="(math:floor(math:random() * $range) mod $range) + ./@from"/>
  </xsl:template>

  <xsl:template match="d:integer">
    <xsl:apply-templates select="./d:*"/>

    <xsl:if test="not(./d:*)">
      <xsl:call-template name="random-digit">
        <xsl:with-param name="size">1</xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="d:field">
    <xsl:element name="field">
      <xsl:attribute name="name">
        <xsl:value-of select="./@name"/>
      </xsl:attribute>
      <xsl:attribute name="type">
        <xsl:value-of select="./@type"/>
      </xsl:attribute>
      <xsl:apply-templates select="./d:integer"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="/">
    <xsl:element name="testdata">
      <xsl:apply-templates select="/d:data/d:field"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
